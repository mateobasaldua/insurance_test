from unittest import TestCase
from unittest import mock
from app import app

from controllers import ResourceController
from models import Resource


class InsuranceTestCase(TestCase):

    def setUp(self):
        self.app = app.test_client()
        self.resources = [Resource('http://test1.com'), Resource('http://test2.com'), Resource('http://test3.com')]

    def test_complete_flow(self):
        results = [{'deductible': 1, 'stop_loss': 5, 'oop_max': 3},
                   {'deductible': 2, 'stop_loss': 6, 'oop_max': 4},
                   {'deductible': 3, 'stop_loss': 7, 'oop_max': 5}]

        with mock.patch('controllers.ResourceController.get_active_resources', return_value=self.resources):
            with mock.patch('controllers.ResourceController.request_info', side_effect=results):
                response = self.app.get('/insurance_information')
                self.assertEqual(response.status_code, 200)
                self.assertEqual(eval(response.data), {'deductible': 2, 'stop_loss': 6, 'oop_max': 4})


class ResourceControllerTestCase(TestCase):

    def test_get_good_request(self):
        resources = ResourceController.get_active_resources()
        controller = ResourceController(resources[0])
        info = controller.request_info()
        self.assertEqual(len(info), 30)

    def test_get_bad_request(self):
        resource = Resource('https://httpbin.org/status/404')
        controller = ResourceController(resource)
        with self.assertRaises(RuntimeError) as context:
            controller.request_info()
            self.assertFalse(resource.is_active())

    def test_get_active_resources(self):
        resources = ResourceController.get_active_resources()
        self.assertEqual(len(resources), 1)
        self.assertEqual(resources[0].url, 'https://api.github.com/events')
