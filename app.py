import threading
import queue
import logging
import time

from flask import Flask, jsonify
from controllers import ResourceController

app = Flask(__name__)

logging.basicConfig(level=logging.DEBUG, format='(%(threadName)-9s) ==> %(message)s')

q = queue.Queue()


class ProducerThread(threading.Thread):
    def __init__(self, controller=None, name=None):
        super(ProducerThread, self).__init__()
        self.controller = controller
        self.name = name

    def run(self):
        try:
            response = self.controller.request_info()
            time.sleep(2)
        except RuntimeError as e:
            logging.exception(str(e))
            return

        q.put(response)
        logging.info('Putting resource {} to queue with {} items'.format(self.controller.resource, q.qsize()))


@app.route('/insurance_information')
def insurance_information():
    resources = ResourceController.get_active_resources()
    producers = [ProducerThread(controller=ResourceController(resource), name='Producer').start()
                 for resource in resources]

    responses = []
    while producers:
        item = q.get()
        responses.append(item)
        logging.info('Getting item {} from queue with {} items'.format(item, q.qsize()))
        producers.pop(0)

    result = {'deductible': 0, 'stop_loss': 0, 'oop_max': 0}
    for response in responses:
        result['deductible'] += response['deductible']
        result['stop_loss'] += response['stop_loss']
        result['oop_max'] += response['oop_max']

    return jsonify({'deductible': round(result['deductible']/len(responses), 2),
                    'stop_loss': round(result['stop_loss']/len(responses), 2),
                    'oop_max': round(result['oop_max']/len(responses), 2)})
