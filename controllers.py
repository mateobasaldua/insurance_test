import requests
from models import Resource
from requests.exceptions import HTTPError


class ResourceController:
    def __init__(self, resource):
        self.resource = resource

    def request_info(self):
        response = requests.get(self.resource.url)
        try:
            response.raise_for_status()
        except HTTPError as _:
            self.resource.deactivate()
            raise RuntimeError('Bad request on {}'.format(self.resource))

        return response.json()

    @classmethod
    def get_active_resources(cls):
        # TODO: bring the resources from database using sqlalchemy
        # Now just mocking
        return [Resource('https://api.github.com/events')]
