# here should be declarated the models using SQLAlchemy, but I am going to just use memory


class Resource:
    def __init__(self, url):
        self.url = url
        self.active = True

    def __str__(self):
        return self.url

    def is_active(self):
        return self.active

    def deactivate(self):
        self.active = False
